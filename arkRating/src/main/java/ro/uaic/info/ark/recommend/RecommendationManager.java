package ro.uaic.info.ark.recommend;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import ro.uaic.info.ark.datastore.QueryExecutor;
import ro.uaic.info.ark.datastore.crypto.Hasher;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radu on 2/1/2017.
 */
public class RecommendationManager {

    private final QueryExecutor internalDbQueryExecutor;

    private final String fusekiEndpoint;

    public RecommendationManager(QueryExecutor internalDbQueryExecutor, String fusekiEndpoint) {
        this.internalDbQueryExecutor = internalDbQueryExecutor;
        this.fusekiEndpoint = fusekiEndpoint;
    }

    public void addUserRecommendation(String userEmail, String author) {
        String userName = "ark:" + Hasher.getHash(userEmail);
        String createQuery = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "\n" +
                "INSERT DATA {\n" +
                "  " + userName + " ark:interestedIn" + author + ".\n" +
                "}\n";
        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                UpdateFactory.create(createQuery), fusekiEndpoint);
        processor.execute();
    }

    public List<String> getUserRecommendations(String userEmail) {
        String userName = "ark:" + Hasher.getHash(userEmail);
        String selectQuery = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "\n" +
                "SELECT ?author WHERE {\n" +
                "  " + userName + " ark:interestedIn ?author.\n" +
                "}\n";


        ParameterizedSparqlString qs = new ParameterizedSparqlString(selectQuery);
        ResultSet set = internalDbQueryExecutor.executeQuery(qs.asQuery());
        List<String> authors = new ArrayList<String>();
        while (set.hasNext()) {
            QuerySolution solution = set.next();
            authors.add(solution.get("author").toString());
        }
        return authors;
    }
}
