package ro.uaic.info.ark.rating;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import ro.uaic.info.ark.datastore.QueryExecutor;
import ro.uaic.info.ark.datastore.crypto.Hasher;

import javax.xml.transform.Result;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radu on 1/31/2017.
 */
public class RatingManager {

    private static final String FUSEKI_STRING_QUERY = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "\n" +
            "select ?rating where {\n" +
            "  ?user ark:hasRating ?rating.\n" +
            "}\n";

    private static final String FUSEKI_ARTWORK_QUERY = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "\n" +
            "select ?rating where {\n" +
            "  ?artworkId ark:hasRating ?rating.\n" +
            "}\n";

    private static final String FUSEKI_GET_USERS_QUERY = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "\n" +
            "select ?user where {\n" +
            "  ?user ark:edit ?artworkId.\n" +
            "}\n";

    private final QueryExecutor internalDbQueryExecutor;

    private final String fusekiEndpoint;

    public RatingManager(QueryExecutor internalDbQueryExecutor, String fusekiEndpoint) {
        this.internalDbQueryExecutor = internalDbQueryExecutor;
        this.fusekiEndpoint = fusekiEndpoint;
    }

    public double getUserRating(String userEmail) {
        ResultSet set = getUserRatings(userEmail);
        if (!set.hasNext()) {
            createUserRating(userEmail, 3);
            return 3.0;
        }
        double sum = 0;
        int number = 0;
        while (set.hasNext()) {
            QuerySolution solution = set.next();
            sum += Double.valueOf(solution.get("rating").asLiteral().toString().split("\\^")[0]);
            number++;
        }
        return sum / number;
    }

    public void createUserRating(String userEmail, int rating) {
        String userName = "ark:" + Hasher.getHash(userEmail);

        String createQuery = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "\n" +
                "INSERT DATA {\n" +
                "  " + userName + " ark:hasRating " + String.valueOf(rating) + ".\n" +
                "}\n";

        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                UpdateFactory.create(createQuery), fusekiEndpoint);
        processor.execute();
    }

    public void createUserRatingWithId(String id, double rating) {
        String createQuery = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "\n" +
                "INSERT DATA {\n" +
                "  <" + id + "> ark:hasRating " + String.valueOf(rating) + ".\n" +
                "}\n";

        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                UpdateFactory.create(createQuery), fusekiEndpoint);
        processor.execute();
    }

    public double getArtworkRating(long artworkId) {
        String name = "ark:" + String.valueOf(artworkId);
        String actualQuery = FUSEKI_ARTWORK_QUERY.replace("?artworkId", name);

        ParameterizedSparqlString qs = new ParameterizedSparqlString(actualQuery);

        ResultSet set = internalDbQueryExecutor.executeQuery(qs.asQuery());

        if (!set.hasNext()) {
            createArtworkIdRating(artworkId, 4);
            return 4.0;
        }

        double sum = 0;
        int number = 0;
        while (set.hasNext()) {
            QuerySolution solution = set.next();
            sum += Double.valueOf(solution.get("rating").asLiteral().toString().split("\\^")[0]);
            number++;
        }
        return sum / number;
    }

    public List<String> getUsersWhoRatedArtwork(long artworkId) {
        String name = "ark:" + artworkId;
        String actualQuery = FUSEKI_GET_USERS_QUERY.replace("?artworkId", name);

        ParameterizedSparqlString qs = new ParameterizedSparqlString(actualQuery);

        List<String> result = new ArrayList<String>();
        ResultSet set = internalDbQueryExecutor.executeQuery(qs.asQuery());
        while (set.hasNext()) {
            QuerySolution solution = set.next();
            result.add(solution.get("user").toString());
        }
        return result;
    }

    public void createArtworkIdRatingFromUser(long artworkId, String userEmail, double rating) {
        String artworkName = "ark:" + artworkId;
        String userName = "ark:" + Hasher.getHash(userEmail);

        String createQuery = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "\n" +
                "INSERT DATA {\n" +
                "  " + artworkName + " ark:hasRating " + String.valueOf(rating) + ".\n" +
                "  " + userName + " ark:edit " + artworkName + ".\n" +
                "}\n";

        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                UpdateFactory.create(createQuery), fusekiEndpoint);
        processor.execute();
    }

    public double createArtworkIdRating(long artworkId, double rating) {
        String artworkName = "ark:" + artworkId;

        String createQuery = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "\n" +
                "INSERT DATA {\n" +
                "  " + artworkName + " ark:hasRating " + String.valueOf(rating) + ".\n" +
                "}\n";

        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                UpdateFactory.create(createQuery), fusekiEndpoint);
        processor.execute();
        return getArtworkRating(artworkId);
    }

    private ResultSet getUserRatings(String userEmail) {
        String userName = "ark:" + Hasher.getHash(userEmail);
        String actualQuery = FUSEKI_STRING_QUERY.replace("?user", userName);

        ParameterizedSparqlString qs = new ParameterizedSparqlString(actualQuery);

        return internalDbQueryExecutor.executeQuery(qs.asQuery());
    }

    public void deleteArtworkRating(long artworkId) {
        String name = "ark:" + String.valueOf(artworkId);
        String deleteQuery = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "\n" +
                "DELETE DATA {\n";
        List<Double> ratings = getArtworkRatings(artworkId);
        for (Double rate : ratings) {
            deleteQuery +=  "  " + name + " ark:hasRating " + String.valueOf(rate) + ".\n";
        }
        deleteQuery += "}\n";
        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                UpdateFactory.create(deleteQuery), fusekiEndpoint);
        processor.execute();
    }

    private List<Double> getArtworkRatings(long artworkId) {
        String name = "ark:" + String.valueOf(artworkId);
        String actualQuery = FUSEKI_ARTWORK_QUERY.replace("?artworkId", name);

        ParameterizedSparqlString qs = new ParameterizedSparqlString(actualQuery);

        ResultSet set = internalDbQueryExecutor.executeQuery(qs.asQuery());
        List<Double> result = new ArrayList<Double>();
        while (set.hasNext()) {
            QuerySolution solution = set.next();
            result.add(Double.valueOf(solution.get("rating").asLiteral().toString().split("\\^")[0]));
        }
        return result;
    }
}
