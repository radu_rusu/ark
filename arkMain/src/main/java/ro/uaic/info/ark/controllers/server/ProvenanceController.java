package ro.uaic.info.ark.controllers.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.uaic.info.ark.auth.GoogleAuth;
import ro.uaic.info.ark.manager.ProvenanceManager;
import ro.uaic.info.ark.datastore.model.ProvenanceLine;
import ro.uaic.info.ark.rating.RatingManager;

import java.util.List;

/**
 * Created by Radu on 1/31/2017.
 */
@Controller
@RequestMapping("/v1/ark/provenance")
public class ProvenanceController {

    @Autowired
    @Qualifier("provenanceManager")
    private ProvenanceManager provenanceManager;

    @Autowired
    @Qualifier("googleAuthenticator")
    private GoogleAuth googleAuthenticator;

    @Autowired
    @Qualifier("ratingManager")
    private RatingManager ratingManager;

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<ProvenanceLine> getProvenanceById(@PathVariable("artworkId") long artworkId) {
        return provenanceManager.getProvenanceByArtworkId(artworkId);
    }

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Long> createProvenanceById(@PathVariable("artworkId") long artworkId,
                                                     @RequestParam("userId") String userId,
                                                     @RequestParam("provenanceLines") List<ProvenanceLine> provenances) {
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<Long>(0L, HttpStatus.UNAUTHORIZED);
        }

        String email = googleAuthenticator.getGoogleInfo(userId).getEmail();
        double userRating = ratingManager.getUserRating(email);
        ratingManager.createArtworkIdRatingFromUser(artworkId, email, userRating);

        return new ResponseEntity<Long>(provenanceManager.createProvenanceByArtworkId(userId, artworkId, provenances),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.DELETE, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Long> deleteProvenanceById(@PathVariable("artworkId") long artworkId,
                                                     @RequestParam("userId") String userId) {
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<>(artworkId, HttpStatus.UNAUTHORIZED);
        }
        String email = googleAuthenticator.getGoogleInfo(userId).getEmail();
        double userRating = ratingManager.getUserRating(email);
        provenanceManager.deleteProvenanceByArtworkId(artworkId);
        return new ResponseEntity<>(0L, HttpStatus.OK);
    }

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Long> updateProvenanceById(@PathVariable("artworkId") long artworkId,
                                                     @RequestParam("userId") String userId,
                                                     @RequestParam("provenanceLines") List<ProvenanceLine> provenances) {
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<Long>(0L, HttpStatus.UNAUTHORIZED);
        }
        String email = googleAuthenticator.getGoogleInfo(userId).getEmail();
        double userRating = ratingManager.getUserRating(email);
        ratingManager.createArtworkIdRatingFromUser(artworkId, email, userRating);
        return new ResponseEntity<Long>(provenanceManager.updateProvenanceByArtworkId(userId, artworkId, provenances),
                HttpStatus.OK);
    }


}
