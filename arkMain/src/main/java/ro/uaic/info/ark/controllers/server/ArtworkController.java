package ro.uaic.info.ark.controllers.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.uaic.info.ark.auth.GoogleAuth;
import ro.uaic.info.ark.auth.GoogleInfo;
import ro.uaic.info.ark.manager.ArtworkManager;
import ro.uaic.info.ark.model.main.artwork.CreateArtworkResponse;
import ro.uaic.info.ark.model.main.artwork.GetArtworkByIdResponse;
import ro.uaic.info.ark.rating.RatingManager;
import ro.uaic.info.ark.recommend.RecommendationManager;

import java.util.logging.Logger;

/**
 * Created by Radu on 1/29/2017.
 */
@Controller
@RequestMapping("/v1/ark/artwork")
public class ArtworkController {

    Logger logger = Logger.getLogger(ArtworkController.class.toString());

    @Autowired
    @Qualifier("artworkManager")
    private ArtworkManager artworkManager;

    @Autowired
    @Qualifier("googleAuthenticator")
    private GoogleAuth googleAuthenticator;

    @Autowired
    @Qualifier("ratingManager")
    private RatingManager ratingManager;

    @Autowired
    @Qualifier("recommendationManager")
    private RecommendationManager recommendationManager;

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetArtworkByIdResponse getArtworkById(@RequestParam(value = "userId", required = false) String userId,
                                                 @PathVariable("artworkId") long artworkId) {
        GetArtworkByIdResponse response = artworkManager.getArtworkById(artworkId);
        if (userId != null && googleAuthenticator.isTokenValid(userId)) {
            GoogleInfo info = googleAuthenticator.getGoogleInfo(userId);
            recommendationManager.addUserRecommendation(info.getEmail(), response.getAuthor());
        }
        return artworkManager.getArtworkById(artworkId);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<CreateArtworkResponse> createArtwork(@RequestParam("artworkName") String artworkName,
                                                               @RequestParam("artworkPicture") String picture,
                                                               @RequestParam("artworkAuthor") String author,
                                                               @RequestParam("artworkOrigin") String origin,
                                                               @RequestParam("artworkAbstract") String abs,
                                                               @RequestParam("userId") String userId) {
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<>((CreateArtworkResponse) null, HttpStatus.UNAUTHORIZED);
        }

        String name = googleAuthenticator.getGoogleInfo(userId).getName();

        long artworkId = artworkManager.createArtwork(artworkName, picture, author, name, abs);

        String email = googleAuthenticator.getGoogleInfo(userId).getEmail();
        double userRating = ratingManager.getUserRating(email);
        ratingManager.createArtworkIdRatingFromUser(artworkId, email, userRating);
        return new ResponseEntity<>(
                new CreateArtworkResponse(artworkId),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Long> deleteArtwork(@PathVariable("artworkId") long artworkId,
                                              @RequestParam("userId") String userId) {
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<>(artworkId, HttpStatus.UNAUTHORIZED);
        }
        double userRating = ratingManager.getUserRating(googleAuthenticator.getGoogleInfo(userId).getEmail());
        double artworkRating = ratingManager.getArtworkRating(artworkId);
        if (userRating < artworkRating) {
            return new ResponseEntity<>(artworkId, HttpStatus.UNAUTHORIZED);
        }
        artworkManager.deleteArtwork(artworkId);
        ratingManager.deleteArtworkRating(artworkId);
        return new ResponseEntity<>(artworkId, HttpStatus.OK);
    }

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Long> updateArtwork(@PathVariable("artworkId") long artworkId,
                                              @RequestParam("artworkName") String artworkName,
                                              @RequestParam("artworkPicture") String picture,
                                              @RequestParam("artworkAuthor") String author,
                                              @RequestParam("artworkOrigin") String origin,
                                              @RequestParam("artworkAbstract") String abs,
                                              @RequestParam("userId") String userId) {
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<>(0L, HttpStatus.OK);
        }
        String email = googleAuthenticator.getGoogleInfo(userId).getEmail();
        String userName = googleAuthenticator.getGoogleInfo(userId).getName();
        double userRating = ratingManager.getUserRating(email);
        ratingManager.createArtworkIdRatingFromUser(artworkId, email, userRating);

        return new ResponseEntity<>(artworkManager.updateArtwork(artworkId, artworkName, picture, author, userName, abs),
                HttpStatus.ACCEPTED);
    }

}
