package ro.uaic.info.ark.manager;

import com.google.common.base.Preconditions;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import ro.uaic.info.ark.auth.GoogleAuth;
import ro.uaic.info.ark.datastore.QueryExecutor;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by Radu on 1/31/2017.
 */
public class ProvenancesManager {

    private static final String DBPEDIA_QUERY = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n" +
            "PREFIX dbo:    <http://dbpedia.org/ontology/>\n" +
            "PREFIX dbp:\t<http://dbpedia.org/property/>\n" +
            "\n" +
            "select ?resource ?id ?auth where {\n" +
            "  ?resource rdf:type dbpedia-owl:Artwork.\n" +
            "  ?resource dbo:wikiPageID ?id.\n" +
            "  ?resource dbp:title ?title .\n" +
            "  ?resource dbo:abstract ?abs .\n" +
            "  ?resource dbo:author ?auth\n" +
            "  FILTER(REGEX(?title, ?term)||REGEX(?abs, ?term))\n" +
            "}\n" +
            "limit 100";

    private static final String FUSKEI_QUERY = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "\n" +
            "select ?id where {\n" +
            "  ?artwork rdf:type ark:Artwork.\n" +
            "  ?artwork ark:hasArtworkId ?id .\n" +
            "  ?artwork ark:hasArtworkName ?name .\n" +
            "  ?artwork ark:hasAbstract ?abstract .\n" +
            "  FILTER(REGEX(?name, ?term)||REGEX(?abstract, ?term))\n" +
            "}\n" +
            "limit 100";

    private final QueryExecutor dbpediaQueryExecutor;

    private final QueryExecutor internalDbQueryExecutor;

    private final GoogleAuth googleAuth;


    public ProvenancesManager(QueryExecutor dbpediaQueryExecutor, QueryExecutor internalDbQueryExecutor, GoogleAuth googleAuth) {
        this.dbpediaQueryExecutor = Preconditions.checkNotNull(dbpediaQueryExecutor);
        this.internalDbQueryExecutor = Preconditions.checkNotNull(internalDbQueryExecutor);
        this.googleAuth = Preconditions.checkNotNull(googleAuth);
    }

    public List<String> getProvenancesLinks(String query) {
        List<String> dbpediaResult = computeDbpediaResult(query);
        List<String> fusekiResult = computeFusekiResult(query);
        for (String result : dbpediaResult) {
            if (!fusekiResult.contains(result)) {
                fusekiResult.add(result);
            }
        }
        return fusekiResult;
    }

    private List<String> computeFusekiResult(String query) {
        ParameterizedSparqlString qs = new ParameterizedSparqlString(FUSKEI_QUERY);

        qs.setLiteral("term", query);

        ResultSet set = internalDbQueryExecutor.executeQuery(qs.asQuery());

        return createResultFromResultSet(set);
    }

    private List<String> computeDbpediaResult(String query) {
        ParameterizedSparqlString qs = new ParameterizedSparqlString(DBPEDIA_QUERY);

        qs.setLiteral("term", query);

        ResultSet set = dbpediaQueryExecutor.executeQuery(qs.asQuery());

        return createResultFromResultSet(set);
    }

    private List<String> createResultFromResultSet(ResultSet set) {
        String link;
        try {
            link = "http://" + InetAddress.getLocalHost().getHostName() + ":8080" + "/v1/ark/provenance/";
        } catch (UnknownHostException e) {
            throw new RuntimeException("Unknown host exception");
        }
        List<String> result = new ArrayList<>();
        while (set.hasNext()) {
            QuerySolution sol = set.next();
            Long id = Long.valueOf(sol.get("id").asLiteral().toString().split("\\^")[0]);
            result.add(link + String.valueOf(id));
        }
        Set<String> hashSet = new HashSet<>();
        hashSet.addAll(result);
        result.clear();
        result.addAll(hashSet);
        return result;
    }

}
