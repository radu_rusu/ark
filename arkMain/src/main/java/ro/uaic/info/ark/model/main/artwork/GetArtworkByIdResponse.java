package ro.uaic.info.ark.model.main.artwork;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Radu on 1/29/2017.
 */
public class GetArtworkByIdResponse {

    private final String artworkName;

    private final String artworkAbstract;

    private final String picture;

    private final String author;

    private final String source;


    public GetArtworkByIdResponse(String artworkName, String artworkAbstract,
                                  String picture, String author, String source) {
        Preconditions.checkArgument(StringUtils.isNotBlank(artworkName));
        Preconditions.checkArgument(StringUtils.isNotBlank(author));
        this.artworkName = artworkName;
        this.artworkAbstract = artworkAbstract;
        this.picture = picture;
        this.author = author;
        this.source = source;
    }

    public String getArtworkName() {
        return artworkName;
    }

    public String getArtworkAbstract() {
        return artworkAbstract;
    }

    public String getPicture() {
        return picture;
    }

    public String getAuthor() {
        return author;
    }

    public String getSource() {
        return source;
    }
}
