package ro.uaic.info.ark.controllers.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.uaic.info.ark.manager.ProvenancesManager;

import java.util.List;

/**
 * Created by Radu on 1/31/2017.
 */
@Controller
@RequestMapping("/v1/ark/provenances")
public class ProvenancesController {

    @Autowired
    @Qualifier("provenancesManager")
    private ProvenancesManager provenancesManager;

    @RequestMapping(value = "/{queryString}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getProvenances(@PathVariable("queryString") String queryString) {
        return provenancesManager.getProvenancesLinks(queryString);
    }
}
