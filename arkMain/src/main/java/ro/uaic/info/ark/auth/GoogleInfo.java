package ro.uaic.info.ark.auth;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Radu on 1/31/2017.
 */
public class GoogleInfo {

    private final String email;

    private final String name;

    public GoogleInfo(String email, String name) {
        Preconditions.checkArgument(StringUtils.isNotBlank(email));
        Preconditions.checkArgument(StringUtils.isNotBlank(name));
        this.email = email;
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }
}
