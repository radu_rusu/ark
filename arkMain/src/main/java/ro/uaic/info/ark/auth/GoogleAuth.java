package ro.uaic.info.ark.auth;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.testing.json.MockJsonFactory;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by Radu on 1/30/2017.
 */
public class GoogleAuth {

    private final String cliend_id;
    private final GoogleIdTokenVerifier verifier;

    public GoogleAuth(String client_id) {
        Preconditions.checkArgument(StringUtils.isNotBlank(client_id));
        this.cliend_id = client_id;
        verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singletonList(cliend_id))
                .build();
    }

    public boolean isTokenValid(String idTokenString) {
        try {
            GoogleIdToken idToken = verifier.verify(idTokenString);
            return idToken != null;
        } catch (GeneralSecurityException | IOException e) {
            return false;
        }
    }

    public GoogleInfo getGoogleInfo(String idTokenString) {
        try {
            GoogleIdToken idToken = verifier.verify(idTokenString);
            GoogleIdToken.Payload payload = idToken.getPayload();
            String name = (String)payload.get("given_name") + " " + (String)payload.get("family_name");
            return new GoogleInfo(payload.getEmail(), name);
        } catch (GeneralSecurityException | IOException e) {
            //should not happen
            return new GoogleInfo("e", "n");
        }
    }
}
