package ro.uaic.info.ark.model.main.artwork;

/**
 * Created by Radu on 1/30/2017.
 */
public class CreateArtworkResponse {

    private final long artworkId;

    public CreateArtworkResponse(long artworkId) {
        this.artworkId = artworkId;
    }


    public long getArtworkId() {
        return artworkId;
    }
}
