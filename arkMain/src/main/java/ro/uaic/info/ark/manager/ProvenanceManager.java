package ro.uaic.info.ark.manager;

import com.google.api.client.repackaged.com.google.common.base.Preconditions;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import ro.uaic.info.ark.auth.GoogleAuth;
import ro.uaic.info.ark.auth.GoogleInfo;
import ro.uaic.info.ark.datastore.DatastoreUpdater;
import ro.uaic.info.ark.datastore.QueryExecutor;
import ro.uaic.info.ark.datastore.model.ProvenanceLine;
import ro.uaic.info.ark.model.main.artwork.ProvenanceAction;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Radu on 1/31/2017.
 */
public class ProvenanceManager {

    private static final String DBPEDIA_QUERY = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n" +
            "PREFIX dbo:    <http://dbpedia.org/ontology/>\n" +
            "PREFIX dbp: <http://dbpedia.org/property/>\n" +
            "PREFIX prov: <http://www.w3.org/ns/prov#>\n" +
            "\n" +
            "select ?auth ?origin where {\n" +
            "  ?resource rdf:type dbpedia-owl:Artwork.\n" +
            "  ?resource dbo:wikiPageID ?id .\n" +
            "  ?resource dbo:author ?auth .\n" +
            "  ?resource prov:wasDerivedFrom ?origin\n" +
            "}\n" +
            "limit 100";

    private static final String FUSEKI_QUERY = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
            "\n" +
            "select ?action ?owner ?contributor ?startYear ?endYear where {\n" +
            "  ?artworkName ark:hasProvenanceLine ?provenanceName.\n" +
            "  ?provenanceName ark:hasProvenanceAction ?action .\n" +
            "  ?provenanceName ark:hasProvenanceOwner ?owner .\n" +
            "  ?provenanceName ark:hasProvenanceContributor ?contributor .\n" +
            "  ?provenanceName ark:hasProvenanceStartYear ?startYear .\n" +
            "  ?provenanceName ark:hasProvenanceEndYear ?endYear .\n" +
            "}\n";

    private final QueryExecutor dbpediaQueryExecutor;

    private final QueryExecutor internalDbQueryExecutor;

    private final DatastoreUpdater datastore;

    private final GoogleAuth googleAuth;


    public ProvenanceManager(QueryExecutor dbpediaQueryExecutor, QueryExecutor internalDbQueryExecutor,
                             DatastoreUpdater datastore, GoogleAuth googleAuth) {
        this.dbpediaQueryExecutor = Preconditions.checkNotNull(dbpediaQueryExecutor);
        this.internalDbQueryExecutor = internalDbQueryExecutor;
        this.datastore = datastore;
        this.googleAuth = googleAuth;
    }

    public List<ProvenanceLine> getProvenanceByArtworkId(long artworkId) {
        List<ProvenanceLine> fusekiResult = getProvenanceByArtworkIdFromFuseki(artworkId);
        ProvenanceLine provenanceLine = getProvenanceFromDbpedia(artworkId);
        fusekiResult.sort((Comparator<ProvenanceLine>) (o1, o2) -> {
            if (o1 == null && o2 != null) {
                return 1;
            }
            if (o1 != null && o2 == null) {
                return -1;
            }
            if (o1 == null && o2 == null) {
                return 0;
            }
            return Integer.valueOf(o1.getStartYear()) - Integer.valueOf(o2.getEndYear());
        });
        if (fusekiResult.size() >= 1 && fusekiResult.get(0).getAction().equals(ProvenanceAction.CREATE.getName())) {
            return fusekiResult;
        }
        if (provenanceLine == null) {
            return fusekiResult;
        }
        fusekiResult.add(0, provenanceLine);
        return fusekiResult;
    }

    private ProvenanceLine getProvenanceFromDbpedia(long artworkId) {
        ParameterizedSparqlString qs = new ParameterizedSparqlString(DBPEDIA_QUERY);

        qs.setLiteral("id", artworkId);
        ResultSet set = dbpediaQueryExecutor.executeQuery(qs.asQuery());
        if (!set.hasNext()) {
            return null;
        }
        QuerySolution solution = set.next();
        String author = solution.get("auth").toString();
        String contributor = solution.get("origin").toString();
        return new ProvenanceLine(ProvenanceAction.CREATE.getName(), author, null, null, contributor);
    }

    private List<ProvenanceLine> getProvenanceByArtworkIdFromFuseki(long artworkId) {
        String artworkName = "ark:" + String.valueOf(artworkId);
        String actualQuery = FUSEKI_QUERY.replace("?artworkName", artworkName);
        ParameterizedSparqlString qs = new ParameterizedSparqlString(actualQuery);

        ResultSet resultSet = internalDbQueryExecutor.executeQuery(qs.asQuery());
        return aggregateFusekiResponse(resultSet);
    }

    private List<ProvenanceLine> aggregateFusekiResponse(ResultSet resultSet) {
        List<ProvenanceLine> response = new ArrayList<>();
        while (resultSet.hasNext()) {
            QuerySolution solution = resultSet.next();
            String action = ProvenanceAction.fromString(solution.get("action").asLiteral().toString()).getName();
            String owner = solution.get("owner").asLiteral().toString();
            String contributor = solution.get("contributor").asLiteral().toString();
            String startYear = solution.get("startYear") != null
                    ? solution.get("startYear").asLiteral().toString() : null;
            String endYear = solution.get("endYear") != null
                    ? solution.get("endYear").asLiteral().toString() : null;
            response.add(new ProvenanceLine(action, owner, startYear, endYear, contributor));
        }
        return response;
    }

    public long createProvenanceByArtworkId(String userId, long artworkId, List<ProvenanceLine> provenances) {
        GoogleInfo googleInfo = googleAuth.getGoogleInfo(userId);
        List<ProvenanceLine> updateProvenance = new ArrayList<>();
        for (ProvenanceLine line : provenances) {
            updateProvenance.add(new ProvenanceLine(line, googleInfo.getName()));
        }
        return datastore.createProvenanceByArtworkId(artworkId, updateProvenance);
    }

    public void deleteProvenanceByArtworkId(long artworkId) {
        List<ProvenanceLine> currentData = getProvenanceByArtworkIdFromFuseki(artworkId);
        datastore.deleteProvenance(artworkId, currentData);
    }

    public long updateProvenanceByArtworkId(String userId, long artworkId, List<ProvenanceLine> provenanceLines) {
        deleteProvenanceByArtworkId(artworkId);
        return createProvenanceByArtworkId(userId, artworkId, provenanceLines);
    }
}
