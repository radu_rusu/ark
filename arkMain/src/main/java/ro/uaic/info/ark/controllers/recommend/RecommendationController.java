package ro.uaic.info.ark.controllers.recommend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ro.uaic.info.ark.auth.GoogleAuth;
import ro.uaic.info.ark.manager.ArtworkManager;
import ro.uaic.info.ark.recommend.RecommendationManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radu on 2/1/2017.
 */
@Controller
@RequestMapping("/v1/ark/recommend")
public class RecommendationController {

    @Autowired
    @Qualifier("recommendationManager")
    private RecommendationManager recommendationManager;

    @Autowired
    @Qualifier("googleAuthenticator")
    private GoogleAuth googleAuthenticator;

    @Autowired
    @Qualifier("artworkManager")
    private ArtworkManager artworkManager;

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<List<String>> getRatingForArtwork(@PathVariable("userId") String userId){
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<List<String>>(new ArrayList<>(), HttpStatus.UNAUTHORIZED);
        }
        List<String> author = recommendationManager.getUserRecommendations(googleAuthenticator.getGoogleInfo(userId).getEmail());
        int rand = (int)(Math.random() * author.size());
        return new ResponseEntity<List<String>>(artworkManager.getArtworksByAuthor(author.get(rand)), HttpStatus.OK);
    }
}
