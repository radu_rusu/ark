package ro.uaic.info.ark.controllers.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ro.uaic.info.ark.datastore.model.ProvenanceLine;
import ro.uaic.info.ark.manager.ArtworkManager;
import ro.uaic.info.ark.manager.ProvenanceManager;
import ro.uaic.info.ark.model.main.artwork.GetArtworkByIdResponse;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class UIController {

    @Autowired
    @Qualifier("artworkManager")
    private ArtworkManager artworkManager;

    @Autowired
    @Qualifier("provenanceManager")
    private ProvenanceManager provenanceManager;

    @RequestMapping(method = RequestMethod.GET)
    public String welcome(ModelMap model) {

        return "first";
    }

    @RequestMapping(value = "results/{queryString}", method = RequestMethod.GET)
    public String secondPage(@PathVariable("queryString") String queryString, ModelMap model) {
        model.put("queryString", queryString);
        return "second";
    }

    @RequestMapping("sparqlEndpoint")
    public String sparqlEndpoint(ModelMap model) {
        return "sparqlEndpoint";
    }

    @RequestMapping(value = "view/{artworkId}", method = RequestMethod.GET)
    public String thirdPage(@PathVariable("artworkId") String queryString, ModelMap model) {
        model.put("artworkId", queryString);
        return "third";
    }

    @RequestMapping(value = "edit/{artworkId}", method = RequestMethod.GET)
    public String editArtwork(@PathVariable("artworkId") long artworkId, ModelMap model) {
        GetArtworkByIdResponse artwork = artworkManager.getArtworkById(artworkId);
        List<ProvenanceLine> provenanceLines = provenanceManager.getProvenanceByArtworkId(artworkId);
        model.put("artworkId", artworkId);
        model.put("artworkName", artwork.getArtworkName());
        model.put("artworkAbstract", artwork.getArtworkAbstract());
        model.put("artworkPictureLink", artwork.getPicture());
        List<ModelMap> list = new ArrayList<>();
        Integer nr = 1;
        for (ProvenanceLine provenance : provenanceLines) {
            ModelMap map = new ModelMap();
            map.put("order", String.valueOf(nr)); nr++;
            map.put("provenanceOwner", provenance.getOwner());
            map.put("provenanceAction", provenance.getAction());
            map.put("provenanceStart", provenance.getStartYear());
            map.put("provenanceEnd", provenance.getEndYear());
            list.add(map);
        }
        model.put("provenances", list);
        model.put("maxOrder", nr - 1);
        return "edit";
    }

}
