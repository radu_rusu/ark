package ro.uaic.info.ark.controllers.rating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.uaic.info.ark.auth.GoogleAuth;
import ro.uaic.info.ark.rating.RatingManager;

import java.util.List;

/**
 * Created by Radu on 1/31/2017.
 */
@Controller
@RequestMapping("/v1/ark/artwork/rating")
public class RatingController {

    @Autowired
    @Qualifier("ratingManager")
    private RatingManager ratingManager;

    @Autowired
    @Qualifier("googleAuthenticator")
    private GoogleAuth googleAuthenticator;

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public double getRatingForArtwork(@PathVariable("artworkId") long artworkId){
        return ratingManager.getArtworkRating(artworkId);
    }

    @RequestMapping(value = "/{artworkId}", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public ResponseEntity<Double> putRatingForArtwork(@PathVariable("artworkId") long artworkId,
                                                      @RequestParam("rating") double rating,
                                                      @RequestParam("userId") String userId) {
        if (!googleAuthenticator.isTokenValid(userId)) {
            return new ResponseEntity<Double>(0.0, HttpStatus.UNAUTHORIZED);
        }

        List<String> users = ratingManager.getUsersWhoRatedArtwork(artworkId);
        double newRating = ratingManager.createArtworkIdRating(artworkId, rating);
        for (String user : users) {
            ratingManager.createUserRatingWithId(user, newRating);
        }
        return new ResponseEntity<Double>(newRating, HttpStatus.OK);
    }
}
