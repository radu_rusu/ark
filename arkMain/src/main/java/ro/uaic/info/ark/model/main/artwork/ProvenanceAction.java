package ro.uaic.info.ark.model.main.artwork;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Radu on 1/31/2017.
 */
public enum ProvenanceAction {
    CREATE("create"),
    DISPLAY("display"),
    OWN("own");

    private final String name;

    private static Map<String, ProvenanceAction> map = new HashMap<>();

    static {
        for (ProvenanceAction action : ProvenanceAction.values()) {
            map.put(action.getName(), action);
        }
    }

    ProvenanceAction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static ProvenanceAction fromString(String name) {
        if (map.containsKey(name)) {
            return map.get(name);
        }
        throw new IllegalArgumentException("No qualifier " + name + ".");
    }
}
