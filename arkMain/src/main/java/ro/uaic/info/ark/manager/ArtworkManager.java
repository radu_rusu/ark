package ro.uaic.info.ark.manager;

import com.google.common.base.Preconditions;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.springframework.security.access.method.P;
import ro.uaic.info.ark.datastore.DatastoreUpdater;
import ro.uaic.info.ark.datastore.QueryExecutor;
import ro.uaic.info.ark.model.main.artwork.GetArtworkByIdResponse;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radu on 1/29/2017.
 */
public class ArtworkManager {

    private static final String DBPEDIA_QUERY = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n" +
            "PREFIX dbo:    <http://dbpedia.org/ontology/>\n" +
            "PREFIX dbp: <http://dbpedia.org/property/>\n" +
            "PREFIX prov: <http://www.w3.org/ns/prov#>\n" +
            "\n" +
            "select ?abs ?title ?origin ?auth where {\n" +
            "  ?resource rdf:type dbpedia-owl:Artwork.\n" +
            "  ?resource dbo:wikiPageID ?id .\n" +
            "  ?resource dbp:title ?title .\n" +
            "  ?resource dbo:abstract ?abs .\n" +
            "  ?resource prov:wasDerivedFrom ?origin .\n" +
            "  ?resource dbo:author ?auth\n" +
            "}\n" +
            "limit 100";

    private static final String FUSKEI_QUERY = "PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
            "\n" +
            "select ?name ?abstract ?picture ?source ?author where {\n" +
            "  ?artwork rdf:type ark:Artwork.\n" +
            "  ?artwork ark:hasArtworkId ?id .\n" +
            "  ?artwork ark:hasArtworkName ?name .\n" +
            "  ?artwork ark:hasAbstract ?abstract .\n" +
            "  ?artwork ark:hasPicture ?picture .\n" +
            "  ?artwork ark:hasOrigin ?source .\n" +
            "  ?artwork ark:author ?author \n" +
            "}\n" +
            "limit 100";;

    private final QueryExecutor dbpediaQueryExecutor;

    private final QueryExecutor internalDbQueryExecutor;

    private final DatastoreUpdater datastore;

    public ArtworkManager(QueryExecutor dbpediaQueryExecutor, QueryExecutor internalDbQueryExecutor,
                          DatastoreUpdater datastore) {
        this.dbpediaQueryExecutor = Preconditions.checkNotNull(dbpediaQueryExecutor);
        this.internalDbQueryExecutor = Preconditions.checkNotNull(internalDbQueryExecutor);
        this.datastore = Preconditions.checkNotNull(datastore);
    }

    public GetArtworkByIdResponse getArtworkById(long artworkId) {

        GetArtworkByIdResponse dbpediaResult = getDbpediaResponse(artworkId);
        GetArtworkByIdResponse internalResult = getInternalResponse(artworkId);

        String artworkName = dbpediaResult.getArtworkName();
        artworkName = artworkName.split("@")[0];
        dbpediaResult = new GetArtworkByIdResponse(
                artworkName, dbpediaResult.getArtworkAbstract(),
                dbpediaResult.getPicture(), dbpediaResult.getAuthor(), dbpediaResult.getSource());

        if (internalResult == null) {
            datastore.addArtwork(artworkId, dbpediaResult.getArtworkName(), "",
                    dbpediaResult.getAuthor(), dbpediaResult.getSource(), dbpediaResult.getArtworkAbstract());
            return dbpediaResult;
        }
        return internalResult;
    }

    public List<String> getArtworksByAuthor(String author) {
        String query =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n" +
                "PREFIX dbo:    <http://dbpedia.org/ontology/>\n" +
                "PREFIX dbp: <http://dbpedia.org/property/>\n" +
                "PREFIX prov: <http://www.w3.org/ns/prov#>\n" +
                "\n" +
                "select ?id where {\n" +
                "  ?resource rdf:type dbpedia-owl:Artwork.\n" +
                "  ?resource dbo:wikiPageID ?id .\n" +
                "  ?resource dbo:author " + author + "\n" +
                "}\n" +
                "limit 2";
        ParameterizedSparqlString qs = new ParameterizedSparqlString(FUSKEI_QUERY);

        ResultSet internalResult = internalDbQueryExecutor.executeQuery(qs.asQuery());
        List<String> result = new ArrayList<>();
        String link;
        try {
            link = "http://" + InetAddress.getLocalHost().getHostName() + ":8080" + "/v1/ark/provenance/";
        } catch (UnknownHostException e) {
            throw new RuntimeException("Unknown host exception");
        }
        while (internalResult.hasNext()) {
            QuerySolution sol = internalResult.next();
            Long id = Long.valueOf(sol.get("id").asLiteral().toString().split("\\^")[0]);
            result.add(link + String.valueOf(id));
        }

        return result;
    }

    private GetArtworkByIdResponse getInternalResponse(long artworkId) {
        ParameterizedSparqlString qs = new ParameterizedSparqlString(FUSKEI_QUERY);

        qs.setLiteral("id", artworkId);

        ResultSet internalResult = internalDbQueryExecutor.executeQuery(qs.asQuery());
        if (!internalResult.hasNext()) {
            return null;
        }
        return getArtworkInfoFromFusekiResult(internalResult);
    }

    private GetArtworkByIdResponse getArtworkInfoFromFusekiResult(ResultSet set) {
        QuerySolution solution = set.next();
        String name = solution.get("name").asLiteral().toString();
        String abs = solution.get("abstract").asLiteral().toString();
        String picture = solution.get("picture").asLiteral().toString();
        String author = solution.get("author").asLiteral().toString();
        String origin = solution.get("source").asLiteral().toString();
        return new GetArtworkByIdResponse(name, abs, picture, author, origin);
    }

    private GetArtworkByIdResponse getDbpediaResponse(long artworkId) {
        ParameterizedSparqlString qs = new ParameterizedSparqlString(DBPEDIA_QUERY);

        qs.setLiteral("id", artworkId);

        ResultSet dbpediaResult = dbpediaQueryExecutor.executeQuery(qs.asQuery());
        return getArtworkInfoFromResultSet(dbpediaResult);
    }

    private GetArtworkByIdResponse getArtworkInfoFromResultSet(ResultSet set) {
        String name = "";
        String abs = "";
        String author = "";
        String source = "";
        QuerySolution solution = set.next();
        abs = solution.get("abs").asLiteral().toString();
        name = solution.get("title").asLiteral().toString();
        author = solution.get("auth").toString();
        source = solution.get("origin").toString();
        return new GetArtworkByIdResponse(name, abs, null, author, source);
    }

    public long createArtwork(String artworkName, String picture, String author, String origin, String abs) {
        return datastore.addArtwork(artworkName, picture, author, origin, abs);
    }

    public void deleteArtwork(long artworkId) {
        GetArtworkByIdResponse resp = getArtworkById(artworkId);
        datastore.deleteArtwork(artworkId, resp.getArtworkName(), resp.getPicture(),
                resp.getAuthor(), resp.getSource(), resp.getArtworkAbstract());
    }

    public long updateArtwork(long artworkId, String artworkName, String picture, String author, String origin, String abs) {
        deleteArtwork(artworkId);
        datastore.addArtwork(artworkId, artworkName, picture, author, origin, abs);
        return artworkId;
    }

}
