<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Ark - Artwork Provenance</title>
  <link rel="stylesheet" type="text/css" href="../../resources/css/first.css">
  <link rel="stylesheet" type="text/css" href="../../resources/css/bootstrap-social.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src ="../../resources/js/welcome.js"></script>
  <meta name="google-signin-client_id" content="1082621584934-bp38bu14grmf0iji4numcfs7pg40v847.apps.googleusercontent.com">
  <script>
      var id_token;
      function onSignIn(googleUser) {
          var profile = googleUser.getBasicProfile();
          id_token = googleUser.getAuthResponse().id_token;
          console.log('id_tok:' + id_token);
      }
      function signOut() {
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
              console.log('User signed out.');
          });
      }
  </script>
</head>

<body>
<div class="container-fluid">
  <div class = "row borderLogin">
    <div class="col-md-10 col-xs-9 arkText">ARK</div>
    <div class="col-md-2 col-xs-3">
      <div class="g-signin2" data-onsuccess="onSignIn" id = loginButton></div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-2 col-xs-1"></div>
    <div class="col-md-8 col-xs-9">
      <div id="custom-search-input">
        <div class="input-group col-md-12 col-xs-12">
          <input type="text" class="form-control input-lg" placeholder="Search" id="searchText"/>
							<span class="input-group-btn">
								<button class="btn btn-info btn-lg" type="button" id="searchButton" onClick="redirect()">
                                  <i class="glyphicon glyphicon-search"></i>
                                </button>
							</span>
        </div>
      </div>
    </div>
    <div class="col-md-2"></div>
  </div>
</div>
</body>
</html>
