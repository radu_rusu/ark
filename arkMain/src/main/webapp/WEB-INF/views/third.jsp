<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
  <title>Ark - Artwork Provenance</title>
  <link rel="stylesheet" type="text/css" href="../../resources/css/third.css">
  <link rel="stylesheet" type="text/css" href="../../resources/css/bootstrap-social.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="../../resources/css/bootstrap.min.css">
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
  <meta name="google-signin-client_id" content="1082621584934-bp38bu14grmf0iji4numcfs7pg40v847.apps.googleusercontent.com">
  <style>
    tr:nth-child(even){background-color: #f2f2f2}
  </style>
  <script src="../../resources/js/jquery.min.js"></script>
  <script src="../../resources/js/bootstrap.min.js"></script>
  <script src="../../resources/js/third.js"></script>
  <script>
      $(document).ready(function() {
          var artworkId = ${artworkId};
          loadInfo(${artworkId});
          onSignIn();
      });
      $(document).on('click', '.dropdown-menu li a', function () {
          var selText = $(this).text();
          $('#dLabel').html("<span>" + selText + "</span>" + "<span class=\"caret\" style='margin-left: 10px;'></span>");
      });
  </script>
</head>
<body>
<div class="container-fluid">
  <div class = "row borderLogin">
    <div class="col-md-3 col-xs-4 arkText">ARK</div>
    <div class="col-md-6 col-xs-7">
      <div id="custom-search-input">
        <div class="input-group col-md-12 col-xs-12">
          <input type="text" class="form-control input-lg" placeholder="Search" />
							<span class="input-group-btn">
								<button class="btn btn-info btn-lg" type="button">
                                  <i class="glyphicon glyphicon-search"></i>
                                </button>
							</span>
        </div>
      </div>
    </div>
    <div class="col-md-2 col-xs-0"></div>
    <div class="col-md-3 col-xs-4">
      <div class="g-signin2" data-onsuccess="onSignIn" id = loginButton style="float:right;"></div>
    </div>
  </div>

  <div class = row style="margin-top: 15px">
    <div class="col-md-8 col-xs-6">
      <div class = "col-md-3 col-xs-2" id="ratingPlaceHolder"><span style="font-size: 170%;">Rating:</span><span style="font-size: 170%; font-weight: bold;">3</span></div>
      <div class = "col-md-5 col-xs-4">
        <div class = "col-md-3 col-xs-2">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" id="dLabel" type="button" data-toggle="dropdown"><span>Rate</span>
                <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
              </ul>
            </div>
          </div>
        <div class = "col-md-2 col-xs-2">
          <button style="background-color: #008744;width: 4em;  height: 2.5em; color:  white;" onclick="updateRating(${artworkId})">Send</button>
        </div>
      </div>
    </div>
    <div class = "col-md-4 col-xs-6">
      <div class="btn-group">
        <%--<button type="button" style = "padding: 10px 20px;font-size: 20px;"><span class="glyphicon glyphicon-plus"></span></button>--%>
        <button type="button" style = "padding: 10px 20px;font-size: 20px;"onclick="editArtwork(${artworkId})"><span class="glyphicon glyphicon-edit "></span></button>
        <%--<button type="button" style = "padding: 10px 20px;font-size: 20px;" onclick="deleteArtwork(${artworkId})"><span class="glyphicon glyphicon-trash"></span></button>--%>
      </div>
    </div>
  </div>

  <div class = "row" style="margin-top: 15px">
    <div class = "col-md-8 col-xs-6">
      <p id= "artworkNamePlace" style = "font-weight: bold;">Name</p>
      <p id = "artworkAbstractPlace">Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum
      lorem ipsum lorem ipsum lore ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>
    </div>
    <div id="artworkPictureLink" class = "col-md-4 col-xs-6 divv">
      <%--<img src="../../resources/images/monalisa.jpg" id="img">--%>
    </div>
  </div>

  <div class="table-responsive" style="margin-top: 15px">
    <table class="table table-bordered" id = tabel>
      <thead>
      <tr>
        <th>Who</th>
        <th>Action</th>
        <th>When</th>
      </tr>
      </thead>
      <tbody id="provenanceTable">
      <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
      </tr>
      <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
      </tr>
      <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
      </tr>
      <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
      </tr>
      </tbody>
    </table>
  </div>

  <div class = "row">
    <div class = "col-md-12 col-xs-12">
      <p id="artworkSources"><span id = sources>Sources:</span></p>
    </div>
  </div>
  <input type = "hidden" id = "artworkIdPlaceHolder" value = "${artworkId}"/>
</div>
</body>
</html>
