<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Ark - Artwork Provenance</title>
  <link rel="stylesheet" type="text/css" href="../../resources/css/second.css">
  <link rel="stylesheet" type="text/css" href="../../resources/css/bootstrap-social.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
  <meta name="google-signin-client_id" content="1082621584934-bp38bu14grmf0iji4numcfs7pg40v847.apps.googleusercontent.com">
</head>

<body>
<div class="container-fluid">
  <div class = "row borderLogin">
    <div class="col-md-3 col-xs-4 arkText">ARK</div>
    <div class="col-md-6 col-xs-7">
      <div id="custom-search-input">
        <div class="input-group col-md-12 col-xs-12">
          <input type="text" class="form-control input-lg" placeholder="Search" value = "${queryString}" id="searchText"/>
							<span class="input-group-btn">
								<button class="btn btn-info btn-lg" type="button" onclick="searchHere()">
                                  <i class="glyphicon glyphicon-search"></i>
                                </button>
							</span>
        </div>
      </div>
    </div>
    <div class="col-md-2 col-xs-0"></div>
    <div class="col-md-3 col-xs-4">
      <div class="g-signin2" data-onsuccess="onSignIn" id = loginButton style="float:right;"></div>
    </div>
  </div>
  <div class = "row" style="padding-bottom: 30px" id = row>
    <div class="col-md-2 col-xs-1"></div>
    <div class="col-md-7 col-xs-8" id = par style="overflow-y:scroll; width: 100%; height: 80%;">
      <input type = "hidden" id = queryString value = "${queryString}"/>
    </div>
    <div class="col-md-3 col-xs-3"></div>
  </div>
</div>
<script src ="../../resources/js/second.js"></script>
</body>
</html>
