<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <title>Ark - Artwork Provenance</title>
    <link rel="stylesheet" type="text/css"
          href="../../resources/css/third.css">
    <link rel="stylesheet" type="text/css"
          href="../../resources/css/bootstrap-social.css">
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
          crossorigin="anonymous">
    <link rel="stylesheet" type="text/css"
          href="../../resources/css/bootstrap.min.css">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <meta name="google-signin-client_id" content="1082621584934-bp38bu14grmf0iji4numcfs7pg40v847.apps.googleusercontent.com">
    <style>
        tr:nth-child(even) {
            background-color: #f2f2f2
        }
    </style>
    <script src="../../resources/js/jquery.min.js"></script>
    <script src="../../resources/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="../../resources/js/dataInit.js"></script>
    <script>
        var id_token;
        var countGlobal = ${maxOrder};
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            id_token = googleUser.getAuthResponse().id_token;
            console.log('id_tok:' + id_token);
        }
    </script>
</head>
<body>

<div class="container-fluid">
    <div class="row borderLogin">
        <div class="col-md-3 col-xs-4 arkText">ARK</div>
        <div class="col-md-6 col-xs-7">
            <div id="custom-search-input">
                <div class="input-group col-md-12 col-xs-12">
                    <input type="text" class="form-control input-lg"
                           placeholder="Search" /> <span class="input-group-btn">
							<button class="btn btn-info btn-lg" type="button">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</span>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-xs-0"></div>
        <div class="col-md-3 col-xs-4">
            <div class="g-signin2" data-onsuccess="onSignIn" id = loginButton style="float:right;"></div>
        </div>
    </div>
    <div style="visibility: hidden">bhcfgdgfhjbjknk</div>
    <div class="page-header">
        <h1>Create new artwork</h1>
    </div>
    <div class="row">
        <div class="col-md-8 col-xs-6">
            <div class="form-group">
                <label for="artworkTitle">Artwork Title:</label> <input type="text"
                                                                        class="form-control" id="artworkTitle" value="${artworkName}">
            </div>
            <div class="form-group">
                <label for="artworkAbstract">Artwork Description:</label>
                <textarea class="form-control" rows="5" id="artworkAbstract">${artworkAbstract}</textarea>
            </div>
        </div>
        <div class="col-md-4 col-xs-6 divv">
            <div class="form-group">
                <label for="artworkPictureLink">Picture link:</label> <input type="text"
                                                                             class="form-control" id="artworkPictureLink" value="${artworkPictureLink}">
            </div>
        </div>
    </div>
    <div style="visibility: hidden">bhcfgdgfhjbjknk</div>
    <div class="table-responsive">
        <table class="table table-bordered" id="provenanceListTable">
            <thead>
            <tr>
                <th>Who</th>
                <th>Action</th>
                <th>Start date</th>
                <th>End date</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${provenances}" var="provline">
                <tr>
                    <td><input type="text" class="form-control " id="who_${provline.order}" value="${provline.provenanceOwner}">
                    </td>
                    <td><input type="text" class="form-control" id="action_${provline.order}" value="${provline.provenanceAction}">
                    <td><input type="date" class="form-control" id="start_${provline.order}" value="${provline.provenanceStart}"></td>
                    <td><input type="date" class="form-control" id="end_${provline.order}" value="${provline.provenanceEnd}"></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <button type="button"
                style="float: right; height: 30px; text-align: center;" onclick="addRow()">
            <span class="glyphicon glyphicon-plus"></span>
        </button>
    </div>
    <button type="button" class="btn btn-primary" style="float: right; margin-top:10px;" onclick="createArt(${artworkId})">Save</button>
</div>
</body>
</html>
