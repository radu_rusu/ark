<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ark - Artwork Provenance</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="../../resources/css/first.css">
<link
	href='http://cdn.jsdelivr.net/g/yasqe@2.2(yasqe.min.css),yasr@2.4(yasr.min.css)'
	rel='stylesheet' type='text/css' />
</head>
<body>
	<div class="container-fluid">
		<div class="row borderLogin">
			<div class="col-md-10 col-xs-9 arkText">ARK</div>
			<div class="col-md-2 col-xs-3"></div>
		</div>
	</div>
	<div class="container">
		<h2>Query the Getty Vocabularies</h2>
		<p>SPARQL Query:</p>
		<div id="yasqe"></div>
		<div id="yasr"></div>
	</div>
	<form>
		<input type="hidden" value="<%= session.getId() %>" id="JsessionId">
	</form>
	<script src='http://cdn.jsdelivr.net/yasr/2.4/yasr.bundled.min.js'></script>
	<script src='http://cdn.jsdelivr.net/yasqe/2.2/yasqe.bundled.min.js'></script>
	<script src="../../resources/js/yasqe.js"></script>
</body>
</html>