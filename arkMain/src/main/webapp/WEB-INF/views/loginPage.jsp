<%--
  Created by IntelliJ IDEA.
  User: Radu
  Date: 6/7/2015
  Time: 11:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %> --%>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<meta name="google-signin-client_id" content="1082621584934-bp38bu14grmf0iji4numcfs7pg40v847.apps.googleusercontent.com">
<script src="js/logins.js"></script>
<link rel="stylesheet" type="text/css" href="../css/style.css">

<html>
<head>
    <title>Fii Ev</title>
    <script>
        var id_token;
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            id_token = googleUser.getAuthResponse().id_token;
            console.log('id_tok:' + id_token);
        }
        function signOut() {
            var auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        }
        function func() {

            $.ajax({
                url: "/v1/ark/artwork/rating/42285273",
                dataType: "json",
                type: "PUT",
                data: {
                    userId: id_token,
                    rating: 2.5
                },
                success: function (data) {
                    alert("bla.");
                },
                error: function (err) {
                    alert("Error when trying to submit email.")
                }
            });
           /* $.ajax({
                url: "/v1/ark/provenance/42285273",
                dataType: "json",
                type: "DELETE",
                data: {
                    userId: id_token
                },
                success: function (data) {
                    alert("bla.");
                },
                error: function (err) {
                    alert("Error when trying to submit email.")
                }
            });*/
/*            var data =  [
                {
                    action: "own",
                    owner: "Raducu",
                    startYear: "1993",
                    endYear:"1995",
                    contributor: "Raducu"
                },
                {
                    action: "display",
                    owner: "Museul",
                    startYear: "1997",
                    endYear:"200",
                    contributor: "Raducu"
                }
            ];
            $.ajax({
                url:"/v1/ark/provenance/42285273" ,
                dataType: "json",
                type: "POST",
                data: {
                    provenanceLines: JSON.stringify(data),
                    userId: id_token
                },
                success: function (data) {
                    alert("bla.");
                },
                error: function (err) {
                    alert("Error when trying to submit email.")
                }
            });*/
        }

    </script>
</head>
<body>
<div class="center">

    <a href="/"> <img id="logo" src="../img/logo.png" alt="FiiEv" /></a>
    <div class="g-signin2" data-onsuccess="onSignIn"></div>

    <a href="#" onclick="signOut();">Sign out</a>
    <a href="#" onclick="func();"> Bla bla</a>

    <div class="login">

        <form method="POST" action="/j_spring_security_check">

            <label for="adminEmail">Email</label>
            <input type="text" name="j_username" id="adminEmail" required/> <br />

            <label for="adminPassword">Password</label>
            <input type="password" name="j_password" id="adminPassword" required/> <br />

            <input type="submit" name="login-btn" value="Login"/>

        </form>

        <a href="/login/reset">Forgot your details?</a>

    </div>
    <div id="loginMessageText">
        ${error}
    </div>
</div>
</body>
</html>
