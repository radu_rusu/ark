var queryString = $('#queryString').val()
var par = $('#par')
var id_token;
$(document).ready(function(){
    search(queryString)
    onSignIn();
});


function searchHere(){
    var searchText = $('#searchText').val()
    par.html("")
    search(searchText)

}

function search(s){
    $.ajax({
        url:'/v1/ark/provenances/' + s,
        type:'get',
        contentType: 'application/json',
        success: function (data) {
            $.each(data, function (i,obj) {
                var len = obj.split('/').length
                var id = obj.split('/')[len-1]
                $.ajax({
                    url:'/v1/ark/artwork/' + id,
                    type:'get',
                    contentType:'application/json',
                    success:function(data){
                        var html = '';
                        html += "<h3><a href='/view/" + id +"'>" + data.artworkName + "</a><p class = 'abstractText'>" + data.artworkAbstract + "</p>"
                        par.append(html)
                    }
                })
            })
        },
        error: function (err) {
            alert("Error when trying to submit email.")
        }

    })
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    id_token = googleUser.getAuthResponse().id_token;
    console.log('id_tok:' + id_token);
}