/**
 * Created by Radu on 1/31/2017.
 */
var id_token;

function loadInfo(artworkId) {
    $.ajax({
        url: "/v1/ark/artwork/" + artworkId,
        dataType: "json",
        type: "GET",
        success: function (data) {
            $('#artworkNamePlace').html(data.artworkName);
            $('#artworkAbstractPlace').html(data.artworkAbstract);
            var source = data.source;
            if (source.substring(0, 4) == "http") {
                source = "<a href=\"" + source +"\">" + source + "</a>";
            }
            $('#artworkSources').append(" " + source + " ");
            var pictureLink = data.picture;
            if (pictureLink != null && pictureLink.substring(0, 4) == "http") {
                $('#artworkPictureLink').html("<img src=\"" + pictureLink + "\">")
            }
        },
        error: function (err) {
            alert("Error when trying to get artwork info.")
        }
    });

    $.ajax({
        url: "/v1/ark/provenance/" + artworkId,
        dataType: "json",
        type: "GET",
        success: function (data) {
            var tableBody = $('#provenanceTable');
            var tableString = "";
            $.each(data, function(i,obj) {
                var owner = obj.owner;
                if (owner.substring(0, 4) == "http") {
                    owner = "<a href=\"" + owner +"\">" + owner + "</a>";
                }
                tableString += "<tr>" +
                    "<th scope=\"row\">" + owner + "</th>" +
                    "<td>" + obj.action + "</td>";
                var startYear = obj.startYear;
                var endYear = obj.endYear;
                var period = ""
                if (startYear == null || startYear == "") {
                    period += "N/A";
                } else {
                    period += startYear;
                }
                period += "-";
                if (endYear == null || endYear == "") {
                    period += "N/A";
                } else {
                    period += endYear;
                }


                 tableString += "<td>" + period +"</td>"+
                    "</tr>";
                var contributor = obj.contributor;
                if (contributor.substring(0, 4) == "http") {
                    contributor = "<a href=\"" + contributor +"\">" + contributor + "</a>";
                }
                $('#artworkSources').append(" " + contributor + " ");
            });
            tableBody.html(tableString);
        },
        error: function (err) {
            alert("Error when trying to get provenance info.")
        }
    });

    $.ajax({
        url: "/v1/ark/artwork/rating/" + artworkId,
        dataType: "json",
        type: "GET",
        success: function (data) {
            $('#ratingPlaceHolder').html("<span style=\"font-size: 170%;\">Rating:</span><span " +
                "style=\"font-size: 170%; font-weight: bold;\">"+ data +"</span>");
        },
        error: function (err) {
            alert("Error when trying to get rating info.")
        }
    });


}

function updateRating(artworkId) {
    var rating = $('#dLabel span').html()
    if (rating == "Rate") {
        alert("Please select a value!");
        return;
    }

    $.ajax({
        url: "/v1/ark/artwork/rating/" + artworkId,
        dataType: "json",
        type: "PUT",
        data: {
            rating: rating,
            userId: id_token
        },
        success: function (data) {
            $.ajax({
                url: "/v1/ark/artwork/rating/" + artworkId,
                dataType: "json",
                type: "GET",
                success: function (data) {
                    $('#ratingPlaceHolder').html("<span style=\"font-size: 170%;\">Rating:</span><span " +
                        "style=\"font-size: 170%; font-weight: bold;\">"+ data +"</span>");
                },
                error: function (err) {
                    alert("Error when trying to get rating info.")
                }
            });
            alert("Your rating has been recorded.");
        },
        error: function (err) {
            alert("Error when trying to record rating");
        }
    });
}

function editArtwork(artworkId) {
    window.location.replace("/edit/" + artworkId);
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    id_token = googleUser.getAuthResponse().id_token;
    console.log('id_tok:' + id_token);
}
