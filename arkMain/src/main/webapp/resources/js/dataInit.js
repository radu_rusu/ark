function addRow(){
    countGlobal++;
	var tbody = $('tbody')
	var html = ''
	html += "<tr><td><input type=\"text\" class=\"form-control \" id=\"who_" + countGlobal + "\"></td><td><input type=\"text\" class=\"form-control\" id=\"action_" + countGlobal +"\">"
	html += "</td><td><input type=\"text\" class=\"form-control\" id=\"start_" + countGlobal + "\"></td><td><input type=\"text\" class=\"form-control\" id=\"end_" + countGlobal + "\"></td></tr>"
	tbody.append(html)
}

function createArt(artworkId) {
	var artworkName = $('#artworkTitle').val();
	var artworkAbstract = $('#artworkAbstract').val();
	var artworkPicture = $('#artworkPictureLink').val();
	var prov = [];
	for (var i = 1; i <= countGlobal; i++) {
		prov.push(
			{
				action: $('#action_' + i).val(),
				owner: $('#who_' + i).val(),
				startYear: $('#start_' + i).val(),
				endYear: $('#end_' + i).val(),
                contributor: "dummy"
			}
		)
	}
	if (artworkName == null || artworkAbstract == null || artworkPicture == null) {
		alert('Complete all fields!');
	}
	for (i = 0; i < countGlobal; i++) {
		if (prov[i].action == null || prov[i].owner == null || prov[i].startYear == null || prov[i].endYear == null) {
            alert('Complete all fields!');
            return;
		}
	}
    $.ajax({
        url:'/v1/ark/artwork/' + artworkId,
        type:'PUT',
		dataType: 'json',
		data: {
            artworkName: artworkName,
            artworkAbstract: artworkAbstract,
            artworkPicture: artworkPicture,
            artworkAuthor: "Dummy",
            artworkOrigin: "blank",
			userId: id_token
		},
        success: function (data) {
            $.ajax({
                url:'/v1/ark/provenance/' + artworkId,
                type:'PUT',
                dataType:'json',
                data: {
                    provenanceLines: JSON.stringify(prov),
					userId: id_token
                },
                success: function (data) {
                    window.location.replace("/view/" + artworkId);
                },
                error: function (err) {
					alert("1")
                }

            });
        },
        error: function (err) {
			alert("2")
        }

    })

}