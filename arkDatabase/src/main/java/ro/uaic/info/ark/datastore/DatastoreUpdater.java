package ro.uaic.info.ark.datastore;

import ro.uaic.info.ark.datastore.model.ProvenanceLine;

import java.util.List;

/**
 * Created by Radu on 1/30/2017.
 */
public interface DatastoreUpdater {

    long addArtwork(String artworkName, String picture, String author, String origin, String abs);

    long addArtwork(long artworkId, String artworkName, String picture, String author, String origin, String abs);

    void deleteArtwork(long artworkId, String artworkName, String picture, String author, String origin, String abs);

    long createProvenanceByArtworkId(long artworkId, List<ProvenanceLine> provenances);

    void deleteProvenance(long artworkId, List<ProvenanceLine> provenances);
}
