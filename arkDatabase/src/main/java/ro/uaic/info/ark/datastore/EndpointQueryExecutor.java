package ro.uaic.info.ark.datastore;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.*;

/**
 * Created by Radu on 1/29/2017.
 */
public class EndpointQueryExecutor implements QueryExecutor {

    private final String endpoint;

    public EndpointQueryExecutor(String endpoint) {
        Preconditions.checkArgument(StringUtils.isNotBlank(endpoint), "Endpoint must not be null!");
        this.endpoint = endpoint;
    }

    public ResultSet executeQuery(Query query) {
        QueryExecution exec = QueryExecutionFactory.sparqlService(endpoint, query);

        return ResultSetFactory.copyResults(exec.execSelect());
    }
}
