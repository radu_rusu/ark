package ro.uaic.info.ark.datastore.crypto;

import com.google.common.hash.Hashing;

import java.nio.charset.Charset;

/**
 * Created by Radu on 1/31/2017.
 */
public class Hasher {
    private Hasher() {
    }

    public static String getHash(String stringToBeEncoded) {
        return Hashing.sha256()
                .hashString(stringToBeEncoded, Charset.defaultCharset())
                .toString();
    }
}
