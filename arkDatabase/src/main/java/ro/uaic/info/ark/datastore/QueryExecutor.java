package ro.uaic.info.ark.datastore;

import org.apache.jena.query.Query;
import org.apache.jena.query.ResultSet;

/**
 * Created by Radu on 1/29/2017.
 */
public interface QueryExecutor {

    ResultSet executeQuery(Query query);
}
