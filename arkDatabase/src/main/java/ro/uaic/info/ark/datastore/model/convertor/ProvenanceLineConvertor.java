package ro.uaic.info.ark.datastore.model.convertor;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.core.convert.converter.Converter;
import ro.uaic.info.ark.datastore.model.ProvenanceLine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Radu on 1/31/2017.
 */
public class ProvenanceLineConvertor implements Converter<String, List<ProvenanceLine>> {
    public List<ProvenanceLine> convert(String s) {
        List<ProvenanceLine> provenances = new ArrayList<ProvenanceLine>();
        JSONArray array = new JSONArray(s);
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            provenances.add(new ProvenanceLine(object.get("action").toString(),
                    object.get("owner").toString(),
                    object.get("startYear").toString(),
                    object.get("endYear").toString(),
                    object.get("contributor").toString()));
        }


        return provenances;
    }
}
