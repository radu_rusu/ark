package ro.uaic.info.ark.datastore;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.update.*;
import ro.uaic.info.ark.datastore.crypto.Hasher;
import ro.uaic.info.ark.datastore.model.ProvenanceLine;

import java.util.List;
import java.util.UUID;

/**
 * Created by Radu on 1/30/2017.
 */
public class FusekiDatastoreUpdater implements DatastoreUpdater {

    private final String fusekiEndpoint;

    public FusekiDatastoreUpdater(String fusekiEndpoint) {
        Preconditions.checkArgument(StringUtils.isNotBlank(fusekiEndpoint));
        this.fusekiEndpoint = fusekiEndpoint;
    }


    public long addArtwork(String artworkName, String picture, String author, String origin, String abs) {
        long artworkId = UUID.randomUUID().getMostSignificantBits();
        return addArtwork(artworkId, artworkName, picture, author, origin, abs);
    }

    public long addArtwork(long artworkId, String artworkName, String picture,
                           String author, String origin, String abs) {
        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                createUpdateArtworkQuery(artworkId, artworkName, picture, author, origin, abs), fusekiEndpoint);
        processor.execute();
        return artworkId;
    }

    private UpdateRequest createUpdateArtworkQuery(long artworkId, String artworkName,
                                                   String picture, String author, String origin, String abs) {
        String name = (artworkName + String.valueOf(artworkId)).replaceAll("\\s+", "");
        name = "ark:" + name;
        UpdateRequest result = UpdateFactory.create("PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "\n" +
                "INSERT DATA\n" +
                "{\n" +
                "  " + name + " rdf:type ark:Artwork.\n" +
                "  " + name + " ark:hasArtworkId " + String.valueOf(artworkId) + ".\n" +
                "  " + name + " ark:hasArtworkName \"" + artworkName + "\".\n" +
                "  " + name + " ark:hasAbstract \"" + abs + "\".\n" +
                "  " + name + " ark:hasPicture \"" + picture + "\".\n" +
                "  " + name + " ark:hasOrigin \"" + origin + "\".\n" +
                "  " + name + " ark:author \"" + author + "\"\n" +
                "}");
        return result;
    }

    public void deleteArtwork(long artworkId, String artworkName, String picture,
                           String author, String origin, String abs) {
        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                createDeleteArtworkQuery(artworkId, artworkName, picture, author, origin, abs), fusekiEndpoint);
        processor.execute();
    }

    private UpdateRequest createDeleteArtworkQuery(long artworkId, String artworkName,
                                                   String picture, String author, String origin, String abs) {
        String name = (artworkName + String.valueOf(artworkId)).replaceAll("\\s+", "");
        name = "ark:" + name;
        UpdateRequest result = UpdateFactory.create("PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                "\n" +
                "DELETE DATA\n" +
                "{\n" +
                "  " + name + " rdf:type ark:Artwork.\n" +
                "  " + name + " ark:hasArtworkId " + String.valueOf(artworkId) + ".\n" +
                "  " + name + " ark:hasArtworkName \"" + artworkName + "\".\n" +
                "  " + name + " ark:hasAbstract \"" + abs + "\".\n" +
                "  " + name + " ark:hasPicture \"" + picture + "\".\n" +
                "  " + name + " ark:hasOrigin \"" + origin + "\".\n" +
                "  " + name + " ark:author \"" + author + "\"\n" +
                "}");
        return result;
    }

    public long createProvenanceByArtworkId(long artworkId, List<ProvenanceLine> provenances) {
        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                createAddProvenanceQuery(artworkId, provenances), fusekiEndpoint);
        processor.execute();
        return artworkId;
    }

    private UpdateRequest createAddProvenanceQuery(long artworkId, List<ProvenanceLine> provenances) {
        String name = "ark:" + String.valueOf(artworkId);
        StringBuilder queryBuilder = new StringBuilder("PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"+
                "\n" +
                "INSERT DATA\n" +
                "{\n");
        for (ProvenanceLine provenanceLine : provenances) {
            String provName = "ark:" + Hasher.getHash(String.valueOf(artworkId)
                    + provenanceLine.getAction() + provenanceLine.getOwner() + provenanceLine.getContributor()
                    + provenanceLine.getStartYear() + provenanceLine.getEndYear());
            queryBuilder
                    .append(" " + name + " ark:hasProvenanceLine " + provName + ".\n")
                    .append(" " + provName + " ark:hasProvenanceAction \"" + provenanceLine.getAction() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceOwner \"" + provenanceLine.getOwner() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceContributor \"" + provenanceLine.getContributor() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceStartYear \"" + provenanceLine.getStartYear() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceEndYear \"" + provenanceLine.getEndYear() + "\".\n");
        }
        queryBuilder.append("}");

        return UpdateFactory.create(queryBuilder.toString());
    }

    public void deleteProvenance(long artworkId, List<ProvenanceLine> provenances) {
        UpdateProcessor processor = UpdateExecutionFactory.createRemote(
                createDeleteProvenanceQuery(artworkId, provenances), fusekiEndpoint);

        processor.execute();
    }

    private UpdateRequest createDeleteProvenanceQuery(long artworkId, List<ProvenanceLine> provenances) {
        String name = "ark:" + String.valueOf(artworkId);
        StringBuilder queryBuilder = new StringBuilder("PREFIX ark: <http://www.semanticweb.org/radu/ontologies/2017/0/ark#>\n" +
                "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"+
                "\n" +
                "DELETE DATA\n" +
                "{\n");
        for (ProvenanceLine provenanceLine : provenances) {
            String provName = "ark:" + Hasher.getHash(String.valueOf(artworkId)
                    + provenanceLine.getAction() + provenanceLine.getOwner() + provenanceLine.getContributor()
                    + provenanceLine.getStartYear() + provenanceLine.getEndYear());
            queryBuilder
                    .append(" " + name + " ark:hasProvenanceLine " + provName + ".\n")
                    .append(" " + provName + " ark:hasProvenanceAction \"" + provenanceLine.getAction() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceOwner \"" + provenanceLine.getOwner() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceContributor \"" + provenanceLine.getContributor() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceStartYear \"" + provenanceLine.getStartYear() + "\".\n")
                    .append(" " + provName + " ark:hasProvenanceEndYear \"" + provenanceLine.getEndYear() + "\".\n");
        }
        queryBuilder.append("}");

        return UpdateFactory.create(queryBuilder.toString());
    }
}
