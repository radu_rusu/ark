package ro.uaic.info.ark.datastore.model;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Radu on 1/31/2017.
 */
public class ProvenanceLine {
    private final String action;

    private final String owner;

    private final String startYear;

    private final String endYear;

    private final String contributor;


    public ProvenanceLine(String action, String owner,
                          String startYear, String endYear, String contributor) {
        Preconditions.checkArgument(StringUtils.isNotBlank(action));
        Preconditions.checkArgument(StringUtils.isNotBlank(owner));
        this.action = action;
        this.owner = owner;
        this.startYear = startYear;
        this.endYear = endYear;
        this.contributor = contributor;
    }

    public ProvenanceLine(ProvenanceLine other, String contributor) {
        Preconditions.checkArgument(StringUtils.isNotBlank(contributor));
        this.action = other.action;
        this.owner = other.owner;
        this.startYear = other.startYear;
        this.endYear = other.endYear;
        this.contributor = contributor;
    }

    public String getAction() {
        return action;
    }

    public String getOwner() {
        return owner;
    }

    public String getStartYear() {
        return startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public String getContributor() {
        return contributor;
    }
}
