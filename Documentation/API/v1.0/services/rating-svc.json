{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": " Ark Rating Service",
    "license": {
      "name": "MIT"
    }
  },
  "host": "localhost:81",
  "basePath": "/v1/ark",
  "schemes": [
    "http"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/rating/user/{userId}": {
      "get": {
        "summary": "Get the rating for a certain user.",
        "operationId": "getUserRating",
        "tags": [
          "user",
          "rating"
        ],
        "parameters": [
          {
            "name": "userId",
            "in": "path",
            "description": "User id for which the rating should be retrieve.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "A number between 0 and 5 which represents the rating of the user.",
            "schema": {
              "type": "number",
              "format": "double"
            }
          },
          "401": {
            "description": "User is not authorized to perform this operation."
          },
          "404": {
            "description": "User doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "post": {
        "summary": "Creates a new rating for the user specified by userId.",
        "operationId": "createUserRating",
        "tags": [
          "user",
          "rating"
        ],
        "parameters": [
          {
            "name": "userId",
            "in": "path",
            "description": "User id for which the rating should be created.",
            "required": true,
            "type": "string"
          },
          {
            "name": "rating",
            "in": "query",
            "description": "The rating to be created for user.",
            "required": true,
            "type": "number",
            "format": "double"
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the newly created URI rating.",
            "schema": {
              "type": "string"
            }
          },
          "401": {
            "description": "Unauthorized."
          },
          "404": {
            "description": "user doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "put": {
        "summary": "Updates the rating for the user specified by userId.",
        "operationId": "updateUserRating",
        "tags": [
          "user",
          "rating"
        ],
        "parameters": [
          {
            "name": "userId",
            "in": "path",
            "description": "User id for which the rating should be updated.",
            "required": true,
            "type": "string"
          },
          {
            "name": "rating",
            "in": "query",
            "description": "The rating to be added to the current rating of the user.",
            "required": true,
            "type": "number",
            "format": "double"
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the newly created URI rating.",
            "schema": {
              "type": "string"
            }
          },
          "401": {
            "description": "Unauthorized."
          },
          "404": {
            "description": "User doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "delete": {
        "summary": "Delete the rating for the user specified by the userId.",
        "operationId": "deleteUserRating",
        "tags": [
          "user",
          "rating"
        ],
        "parameters": [
          {
            "name": "userId",
            "in": "path",
            "description": "User id for which the rating should be deleted.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "User rating was successfully deleted."
          },
          "401": {
            "description": "Not authorized to perform this operation."
          },
          "404": {
            "description": "User doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    },
    "/rating/provenance/{artworkId}": {
      "get": {
        "summary": "Get the rating for a certain provenance.",
        "operationId": "getProvenanceRating",
        "tags": [
          "provenance",
          "rating"
        ],
        "parameters": [
          {
            "name": "artworkId",
            "in": "path",
            "description": "Artwork id for which the provenance rating should be retrieve.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "A number between 0 and 5 which represents the rating of the provenance.",
            "schema": {
              "type": "number",
              "format": "double"
            }
          },
          "401": {
            "description": "Not authorized to perform this operation."
          },
          "404": {
            "description": "Artwork doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "post": {
        "summary": "Creates a new rating for the provenance specified by artworkId.",
        "operationId": "createProvenanceRating",
        "tags": [
          "provenance",
          "rating"
        ],
        "parameters": [
          {
            "name": "artworkId",
            "in": "path",
            "description": "Artwork id for which the provenance rating should be created.",
            "required": true,
            "type": "string"
          },
          {
            "name": "rating",
            "in": "query",
            "description": "The rating to be added to the current rating of the provenance.",
            "required": true,
            "type": "number",
            "format": "double"
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the newly created URI rating.",
            "schema": {
              "type": "string"
            }
          },
          "401": {
            "description": "Unauthorized."
          },
          "404": {
            "description": "Artwork doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "put": {
        "summary": "Updates the rating for the provenance specified by artworkId.",
        "operationId": "updateProvenanceRating",
        "tags": [
          "user",
          "rating"
        ],
        "parameters": [
          {
            "name": "artworkId",
            "in": "path",
            "description": "Artwork id for which the provenance rating should be updated.",
            "required": true,
            "type": "string"
          },
          {
            "name": "rating",
            "in": "query",
            "description": "The rating to be added to the current rating of the provenance.",
            "required": true,
            "type": "number",
            "format": "double"
          }
        ],
        "responses": {
          "200": {
            "description": "Returns the newly created URI rating.",
            "schema": {
              "type": "string"
            }
          },
          "401": {
            "description": "Unauthorized."
          },
          "404": {
            "description": "Artwork doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      },
      "delete": {
        "summary": "Delete the rating for the provenance specified by the artwork.",
        "operationId": "deleteProvenanceRating",
        "tags": [
          "user",
          "rating"
        ],
        "parameters": [
          {
            "name": "artworkId",
            "in": "path",
            "description": "Provenance id for which the rating should be deleted.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Provenance rating was successfully deleted."
          },
          "401": {
            "description": "Not authorized to perform this operation."
          },
          "404": {
            "description": "Artwork doesn't exists",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "default": {
            "description": "An error has occurred.",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Error": {
      "type": "object",
      "required": [
        "code",
        "message"
      ],
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        }
      }
    }
  }
}