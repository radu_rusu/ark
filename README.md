## Ark (Artwork Provenance)

Create a system (such as Web platform, framework, component) able to model and manage - including operations like query, visualize, recommend - the knowledge about the provenance of each artistic work in conjunction to DBpedia and/or the Romanian heritage. Various statistical information will be available via a SPARQL endpoint by using the Getty vocabularies. See also Museums Open Data and Europeana's Education, Culture, and Sport datasets.
